package com.hjqjl.jiyizhushou;

import android.content.Context;
import android.os.Bundle;
import android.view.Window;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.blankj.utilcode.util.LogUtils;

public abstract class BaseActivity extends AppCompatActivity {
    protected String CurrentClassName;
    protected Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);// 竖屏
        CurrentClassName = getClass().getSimpleName();
        LogUtils.d("BaseActivity===>" + CurrentClassName + "===>");// 打印出当前activity的名字
        mContext = this;
    }
}

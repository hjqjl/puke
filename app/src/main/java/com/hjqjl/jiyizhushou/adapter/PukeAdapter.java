package com.hjqjl.jiyizhushou.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.hjqjl.jiyizhushou.R;

import java.util.ArrayList;


/**
 * 使用万能适配器adapter
 */
public class PukeAdapter extends BaseQuickAdapter<Integer, BaseViewHolder> {
    /**
     * 构造方法，此示例中，在实例化Adapter时就传入了一个List。
     * 如果后期设置数据，不需要传入初始List，直接调用 super(layoutResId); 即可
     */
    public PukeAdapter(ArrayList<Integer> models) {
        super(R.layout.item_pukeallshow, models);
    }


    /**
     * 在此方法中设置item数据
     */
    @Override
    protected void convert(BaseViewHolder helper, Integer item) {
        helper.setImageResource(R.id.iv_pukeallshow_item, item);
    }
}
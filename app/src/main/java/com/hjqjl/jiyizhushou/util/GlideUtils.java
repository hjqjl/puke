package com.hjqjl.jiyizhushou.util;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hjqjl.jiyizhushou.R;

import java.io.File;

/**
 * seiya 创建时间：2017/4/28 16:40
 */

public class GlideUtils {
    /**
     * 网络获取图片请求（将获取的图片放入imageView）
     *
     * @param context   上下文
     * @param url       图片url
     * @param imageView 要加载图片的imageview
     */
    public static void loadImg(Context context, String url, ImageView imageView) {
        if (url == null) {
            url = "";
        }
        Glide.with(context)
                .load(url)
//                .diskCacheStrategy(DiskCacheStrategy.ALL)//如果需要展示多张图，imageview的大小不一样，缓存下一个全图。
//                .centerCrop()
                .placeholder(R.drawable.ic_default_load_bg)
                .error(R.drawable.ic_default_load_bg)
//                .crossFade()
                .into(imageView);

//        Logger.i(context.getClass().getSimpleName() + "--图片url--" + url);
    }

    /**
     * 网络获取图片请求（将获取的图片放入imageView）
     *
     * @param context      上下文
     * @param url          图片url
     * @param defaultResId 默认图片
     * @param imageView    要加载图片的imageview
     */
    public static void loadImg(Context context, String url, int defaultResId, ImageView imageView) {
        if (url == null) {
            url = "";
        }
        Glide.with(context)
                .load(url)
                .placeholder(defaultResId)
                .error(defaultResId)
                .into(imageView);
    }

    /**
     * 网络获取图片请求（将获取的图片放入imageView）
     *
     * @param context   上下文
     * @param file      文件
     * @param imageView 要加载图片的imageview
     */
    public static void loadImg(Context context, File file, ImageView imageView) {
        Glide.with(context)
                .load(file)
                .into(imageView);
    }

    /**
     * @param context   上下文
     * @param res       资源文件
     * @param imageView 要加载图片的imageview
     */
    public static void loadImg(Context context, int res, ImageView imageView) {
        Glide.with(context)
                .load(res)
                .into(imageView);
    }

    /**
     * 不要默认和错误图片
     * 不要缓存
     */
    public static void loadImgNoCache(Context context, String url, ImageView imageView) {
        if (url == null) {
            url = "";
        }
        Glide.with(context)
                .load(url)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(imageView);
    }

    /**
     * 不要默认和错误图片
     * 不要缓存
     * res
     */
    public static void loadImgNoCache(Context context, File file, ImageView imageView) {
        Glide.with(context)
                .load(file)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(imageView);
    }

    /**
     * 不要默认和错误图片
     * 不要缓存
     * res
     */
    public static void loadImgNoCache(Context context, int res, ImageView imageView) {
        Glide.with(context)
                .load(res)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(imageView);
    }
}

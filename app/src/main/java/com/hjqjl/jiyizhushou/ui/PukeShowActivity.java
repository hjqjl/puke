package com.hjqjl.jiyizhushou.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ToastUtils;
import com.hjqjl.jiyizhushou.BaseActivity;
import com.hjqjl.jiyizhushou.R;
import com.hjqjl.jiyizhushou.databinding.ActivityPukeshowBinding;
import com.hjqjl.jiyizhushou.util.GlideUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 2017/11/3.
 */

public class PukeShowActivity extends BaseActivity {
    private int count = 0;
    private ArrayList<Integer> pukeList;
    private Integer[] pukePics = {
            R.drawable.pkp_1, R.drawable.pkp_2, R.drawable.pkp_3, R.drawable.pkp_4, R.drawable.pkp_5
            , R.drawable.pkp_6, R.drawable.pkp_7, R.drawable.pkp_8, R.drawable.pkp_9, R.drawable.pkp_10
            , R.drawable.pkp_11, R.drawable.pkp_12, R.drawable.pkp_13, R.drawable.pkp_14, R.drawable.pkp_15
            , R.drawable.pkp_16, R.drawable.pkp_17, R.drawable.pkp_18, R.drawable.pkp_19, R.drawable.pkp_20
            , R.drawable.pkp_21, R.drawable.pkp_22, R.drawable.pkp_23, R.drawable.pkp_24, R.drawable.pkp_25
            , R.drawable.pkp_26, R.drawable.pkp_27, R.drawable.pkp_28, R.drawable.pkp_29, R.drawable.pkp_30
            , R.drawable.pkp_31, R.drawable.pkp_32, R.drawable.pkp_33, R.drawable.pkp_34, R.drawable.pkp_35
            , R.drawable.pkp_36, R.drawable.pkp_37, R.drawable.pkp_38, R.drawable.pkp_39, R.drawable.pkp_40
            , R.drawable.pkp_41, R.drawable.pkp_42, R.drawable.pkp_43, R.drawable.pkp_44, R.drawable.pkp_45
            , R.drawable.pkp_46, R.drawable.pkp_47, R.drawable.pkp_48, R.drawable.pkp_49, R.drawable.pkp_50
            , R.drawable.pkp_51, R.drawable.pkp_52};

    public static void actionStart(Context context) {
        Intent intent = new Intent(context, PukeShowActivity.class);
        context.startActivity(intent);
    }

    private ActivityPukeshowBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPukeshowBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initData();

        binding.btnPukeshowNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count < 51) {
                    count++;
                    GlideUtils.loadImg(mContext, pukeList.get(count), binding.ivPukeshow);
                    if (count == 51) {
                        ToastUtils.showShort("已是最后一张");
                        binding.btnPukeshowNext.setText("显示所有牌");
                    }
                    binding.tvCount.setText("" + (count + 1));
                } else {
                    PukeAllShowActivity.start(mContext, pukeList);
                }

            }
        });
        binding.btnPukeshowReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initData();
            }
        });
        binding.btnShowAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PukeAllShowActivity.start(mContext, pukeList);
            }
        });
    }

    protected void initData() {
        count = 0;
        List<Integer> list = Arrays.asList(pukePics);
        pukeList = new ArrayList<>(list);
        Collections.shuffle(pukeList);//打乱集合顺序
        GlideUtils.loadImg(mContext, pukeList.get(count), binding.ivPukeshow);

        binding.tvCount.setText("" + (count + 1));
    }
}

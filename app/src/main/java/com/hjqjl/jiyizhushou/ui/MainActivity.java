package com.hjqjl.jiyizhushou.ui;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.hjqjl.jiyizhushou.BaseActivity;
import com.hjqjl.jiyizhushou.databinding.ActivityMainBinding;

public class MainActivity extends BaseActivity {
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.btnMainPuke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PukeShowActivity.actionStart(mContext);
            }
        });
    }
}

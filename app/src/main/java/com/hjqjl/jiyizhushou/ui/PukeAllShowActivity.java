package com.hjqjl.jiyizhushou.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;

import com.hjqjl.jiyizhushou.BaseActivity;
import com.hjqjl.jiyizhushou.R;
import com.hjqjl.jiyizhushou.adapter.PukeAdapter;
import com.hjqjl.jiyizhushou.databinding.ActivityPukeallshowBinding;

import java.util.ArrayList;

/**
 * h --- 2017/11/10.
 */

public class PukeAllShowActivity extends BaseActivity {
    public static void start(Context context, ArrayList<Integer> pukeList) {
        Intent starter = new Intent(context, PukeAllShowActivity.class);
        starter.putIntegerArrayListExtra("list", pukeList);
        context.startActivity(starter);
    }

    ArrayList<Integer> pukeList;

    private ActivityPukeallshowBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPukeallshowBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        pukeList = getIntent().getIntegerArrayListExtra("list");

        PukeAdapter adapter = new PukeAdapter(pukeList);
        GridLayoutManager layoutManager = new GridLayoutManager(mContext,4);
        binding.rlPukeAllshow.setLayoutManager(layoutManager);
        binding.rlPukeAllshow.setAdapter(adapter);
    }
}
